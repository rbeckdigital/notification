#!/bin/bash
if [ -z "$1" ] ; then
    echo "Git password required"
    exit
fi

git pull https://rbeckdigital:$1@bitbucket.org/rbeckdigital/notification.git
vapor build
supervisorctl restart notification
