

$(document).on('submit','#editNotificationForm',function( event ){
               
               
               
               event.preventDefault();
               
               var formData = $(this).serializeArray() ;
               
               var postData = JSON.stringify(
                                             {
                                             id:Number(formData[0].value),
                                             type:formData[1].value,
                                             title:formData[2].value,
                                             description:formData[3].value,
                                             accountName:formData[4].value,
                                             order:Number(formData[5].value),
                                             call2Action:formData[6].value,
                                             call2ActionURL:formData[7].value,
                                             status:formData[8].value,
                                             
                                             
                                             });
               var url = "/notification";
               var type = "post";
               
               console.log(postData);
               
               if ( formData[0].value > 0 ) {url = url+"/"+Number(formData[0].value); type = "patch";}
               
               
               $.ajax
               ({
                
                url: url,
                type: type,
                data: postData,
                processData: false,
                contentType: 'application/json',
                success: function(result){ UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'});
                    location.reload();
                
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    let errorInfo=JSON.parse(XMLHttpRequest.responseText);
                    UIkit.notification('Error: '+errorInfo.reason, {status: 'danger', pos: 'bottom-left'});
                
                }
                
                })
               
               
               });




function markDone( notificationId ){
               
    
               $.ajax
               ({
                
                url: "/notification/done/"+notificationId,
                type: "get",
                processData: false,
                contentType: 'application/json',
                success: function(result){ UIkit.notification(' Marked Done', {status: 'success', pos: 'bottom-left'});
                location.reload();
                
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                let errorInfo=JSON.parse(XMLHttpRequest.responseText);
                UIkit.notification('Error: '+errorInfo.reason, {status: 'danger', pos: 'bottom-left'});
                
                }
                
                })
               
               
               }



function flush(  ){
    
    
    $.ajax
    ({
     
     url: "/notification/flush/",
     type: "get",
     processData: false,
     contentType: 'application/json',
     success: function(result){ UIkit.notification(' Marked Done', {status: 'success', pos: 'bottom-left'});
     location.reload();
     
     },
     error: function(XMLHttpRequest, textStatus, errorThrown){
     let errorInfo=JSON.parse(XMLHttpRequest.responseText);
     UIkit.notification('Error: '+errorInfo.reason, {status: 'danger', pos: 'bottom-left'});
     
     }
     
     })
    
    
    }
