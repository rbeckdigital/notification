//
//  NotificationController.swift
//  App
//
//  Created by Richard Beck on 2019-03-14.
//

import Foundation
import Vapor
import Fluent


final class NotificationController {
    
    func list(_ req: Request) throws -> Future<[Notification]> {
        return Notification.query(on: req).all()
    }
    
    func notification(_ req: Request) throws -> Future<Notification> {
        return Notification.find(try req.parameters.next(Int.self), on:req).map {notification in
            if notification == nil {throw Abort(.notFound)}
            return notification!
        }
    }
    
    func create(_ req: Request) throws -> Future<Notification> {
        return try req.content.decode(Notification.self).flatMap { notification in
              notification.id = nil
            return notification.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Notification.self).flatMap { notification in
            return notification.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<Notification> {
        return Notification.find(try req.parameters.next(Int.self), on: req).flatMap { notification in
            if notification == nil {throw Abort(.notFound)}
            return try req.content.decode(Notification.self).flatMap { newNotification in
                notification?.type = newNotification.type
                notification?.title = newNotification.title
                notification?.description = newNotification.description
                notification?.order = newNotification.order
                notification?.accountName = newNotification.accountName
                notification?.call2Action = newNotification.call2Action
                notification?.call2ActionURL = newNotification.call2ActionURL
                notification?.status = newNotification.status
                
                return (notification?.save(on: req))!
            }
        }
    }
    
    
    
    func updateStatus(_ req: Request) throws -> Future<Notification> {
        return Notification.find(try req.parameters.next(Int.self), on: req).flatMap { notification in
            if notification == nil {throw Abort(.notFound)}
                notification?.status = "done"
                return (notification?.save(on: req))!
            
        }
    }
    
    
    func flush(_ req: Request) throws -> Future<[Notification]> {
        return Notification.query(on: req).all().flatMap { notifications in
            notifications.map { notification -> Future<Notification> in
                    notification.status = "todo"
                    return notification.save(on:req)
            }.flatten(on: req)
        }
    }
    
    
}
