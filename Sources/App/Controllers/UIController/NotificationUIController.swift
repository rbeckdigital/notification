//
//  NotificationUIController.swift
//  App
//
//  Created by Richard Beck on 2019-03-14.
//

import Foundation
import Vapor
import FluentMySQL

final class NotificationUIController {
    
    func renderNotificationList(_ req: Request) throws -> Future<View> {
        return Notification.query(on:req)
            .filter(\.status == "todo")
            .sort(\.order, .ascending).all().flatMap { notifications in
            return try req.view().render("notificationList", ["Notifications":notifications])
        }
    }
    
    
}
