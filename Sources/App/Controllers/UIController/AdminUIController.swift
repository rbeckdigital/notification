//
//  AdminUIController.swift
//  App
//
//  Created by Richard Beck on 2019-03-14.
//

import Foundation
import Vapor
import FluentMySQL

final class AdminUIController {


    func renderAdmin(_ req: Request) throws -> Future<View> {
        return Notification.query(on: req).all().flatMap { notifications in
            return try req.view().render("admin",["Notifications":notifications])
        }
       
    }

    func renderEditNotification(_ req: Request) throws -> Future<View> {
        return Notification.find(try req.parameters.next(Int.self), on:req).flatMap { notification in
              if notification == nil {throw Abort(.notFound)}
             return try req.view().render("adminForm",["Notification":notification!])
            
        }
    
    }
    
    
    func renderCreateNotification(_ req: Request) throws -> Future<View> {
            return try req.view().render("adminForm")
        
    }
    
    func renderLogin(_ req: Request) throws -> Future<View> {
        return try req.view().render("login")
    }
    
    
}
