import Vapor
import Authentication

/// Register your application's routes here.
public func routes(_ router: Router) throws {

    // Security //
    let userController = UserController()
    router.post("setup", use: userController.setUp)
    
    
    let authSessionRouter = router.grouped(User.authSessionsMiddleware())
    let auth = authSessionRouter.grouped(RedirectMiddleware<User>(path: "/login"))
    authSessionRouter.post("login", use: userController.login)
    auth.get("logout", use: userController.logout)
    authSessionRouter.post("tokenLogin", use: userController.tokenLogin)
    auth.get("profile", use: userController.profile)
    authSessionRouter.get("authStatus", use: userController.authStatus)
    authSessionRouter.post("register", use: userController.register)
    
    
    
    // Notifications
    let notificationController = NotificationController()
    auth.get("notification", Int.parameter,use: notificationController.notification)
    auth.post("notification", use: notificationController.create)
    auth.patch("notification", Int.parameter, use: notificationController.update)
    auth.delete("notification", Notification.parameter, use: notificationController.delete)
    auth.get("notification","done" ,Int.parameter, use: notificationController.updateStatus)
    auth.get("notification","flush", use: notificationController.flush)
    
    // AdminUI Controller
    let adminUIController = AdminUIController()
    auth.get("admin",use: adminUIController.renderAdmin)
    auth.get("notification","edit",Int.parameter,use: adminUIController.renderEditNotification)
    auth.get("notification","create",use: adminUIController.renderCreateNotification)
    router.get("login",use: adminUIController.renderLogin)
    
    
    // NotificationUI Controller
    let notificationUIController = NotificationUIController()
    auth.get("",use: notificationUIController.renderNotificationList)
    
    
    
}
