import FluentMySQL
import Vapor
import Leaf
import Authentication
import LeafMarkdown

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    // Register providers first
    try services.register(FluentMySQLProvider())
    try services.register(LeafProvider())
    try services.register(AuthenticationProvider())


    /// Preferences
    config.prefer(MemoryKeyedCache.self, for: KeyedCache.self)
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
    
    
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    /// Leaf Markdown
    var tags = LeafTagConfig.default()
    tags.use(Markdown(), as: "markdown")
    tags.use(uidate(), as: "uidate")
    tags.use(uitags(), as: "uitags")
    tags.use(uidue(), as: "uidue")
    tags.use(uitype(), as: "uitype")
    services.register(tags)
    
    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    middlewares.use(SessionsMiddleware.self)
    middlewares.use(FileMiddleware.self)
    services.register(middlewares)
    
    
    // Configure a MySQL database
    if env.isRelease {
        print("set production access")
        let mysqlConfig = MySQLDatabaseConfig(
            hostname: "bouvmmysql01",
            port: 3306,
            username: "rbeck",
            password: "WDl5NI3F",
            database: "rbeckpoc"
        )
        services.register(mysqlConfig)
    } else {
        print("set local access")
        let mysqlConfig = MySQLDatabaseConfig(
            hostname: "127.0.0.1",
            port: 3306,
            username: "employee",
            password: "employee",
            database: "notifications"
        )
        services.register(mysqlConfig)
    }
  

    // Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: Notification.self, database: .mysql)
    migrations.add(model: User.self, database: .mysql)
    services.register(migrations)
}
