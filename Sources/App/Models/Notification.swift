//
//  Notification.swift
//  App
//
//  Created by Richard Beck on 2019-03-14.
//

import Foundation
import FluentMySQL
import Vapor


final class Notification: MySQLModel {
    var id: Int?
    var type: String
    var title: String
    var accountName: String
    var description: String
    var order:Int?
    var call2Action:String
    var call2ActionURL:String?
    var status:String
    var createdAt: Date?
    var updatedAt: Date?

    
    init(id: Int?, type:String, title: String, accountName:String, description:String, order:Int, call2Action:String, call2ActionURL:String?, status:String) {
        self.type = type
        self.title = title
        self.accountName = accountName
        self.description = description
        self.order = order
        self.call2Action = call2Action
        self.call2ActionURL = call2ActionURL
        self.status = status
        
        
    }
    
    
}


extension Notification: Content {}
extension Notification: Migration {}
extension Notification {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension Notification: Parameter {}
